import { useState, useEffect } from 'react'

function PresentationForm() {
  const [conferences, setConferences] = useState('')
  const [name, setName] = useState('')
  const [email, setEmail] = useState('')
  const [companyName, setCompanyName] = useState('')
  const [title, setTitle] = useState('')
  const [synopsis, setSynopsis] = useState('')
  const [conference, setConference] = useState('')

  async function getConferences(){
    const res = await fetch('http://localhost:8000/api/conferences/')

    if (res.ok){
      const data = await res.json()
      setConferences(data.conferences)
    }
  }

useEffect(()=>{
  getConferences()
}
  ,[])

async function handleSubmit(e){
  e.preventDefault()

  const data = {
    presenter_name: name,
    presenter_email: email,
    company_name: companyName,
    title: title,
    synopsis: synopsis,
  }

  const presentationUrl = `http://localhost:8000${conference}presentations/`;
  console.log(presentationUrl)
  const fetchConfig = {
    method: "post",
    body: JSON.stringify(data),
    headers: {
      'Content-Type': 'application/json',
    },
  };



  const response = await fetch(presentationUrl, fetchConfig);
  if (response.ok) {
    const newLocation = await response.json();
    console.log(newLocation);
    setName('')
    setEmail('')
    setCompanyName('')
    setTitle('')
    setSynopsis('')
    setConference('')
    console.log(companyName)
  }

}

function handleConferenceChange(e){
  setConference(e.target.value)
}

function handleNameChange(e){
  setName(e.target.value)
}

function handleEmailChange(e){
  setEmail(e.target.value)
}

function handleSynopsisChange(e){
  setSynopsis(e.target.value)
}

function handleCompanyNameChange(e){
  setCompanyName(e.target.value)
}

function handleTitleChange(e){
  setTitle(e.target.value)
}


  return ( conferences &&
    <div className="container">
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new presentation</h1>
            <form onSubmit={handleSubmit} id="create-presentation-form">
              <div className="form-floating mb-3">
                <input onChange={handleNameChange} value={name} placeholder="Presenter name" required type="text" name="presenter_name" id="presenter_name" className="form-control"/>
                <label htmlFor="presenter_name">Presenter name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleEmailChange} value={email} placeholder="Presenter email" required type="email" name="presenter_email" id="presenter_email" className="form-control"/>
                <label htmlFor="presenter_email">Presenter email</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleCompanyNameChange} value={companyName} placeholder="Company name" type="text" name="company_name" id="company_name" className="form-control"/>
                <label htmlFor="company_name">Company name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleTitleChange} value={title} placeholder="Title" required type="text" name="title" id="title" className="form-control"/>
                <label htmlFor="title">Title</label>
              </div>
              <div className="mb-3">
                <label htmlFor="synopsis">Synopsis</label>
                <textarea onChange={handleSynopsisChange} value={synopsis} className="form-control" id="synopsis" rows="3" name="synopsis"></textarea>
              </div>
              <div className="mb-3">
                <select onChange={handleConferenceChange} value={conference} required name="conference" id="conference" className="form-select">
                  <option value="">Choose a conference</option>
                  {conferences.map(conf => (
                    <option key={conf.href} value={conf.href}>{conf.name}</option>
                  ))}
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    </div>
  )
}
export default PresentationForm
