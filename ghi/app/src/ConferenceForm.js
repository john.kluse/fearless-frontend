import {useState, useEffect} from 'react';



function ConferenceForm() {
  const [name, setName] = useState('')
  const [starts, setStarts] = useState('')
  const [ends, setEnds] = useState('')
  const [description, setDescription] = useState('')
  const [maxPresentations, setMaxPresentations] = useState('')
  const [maxAttendees, setMaxAttendees] = useState('')
  const [location, setLocation] = useState('')

  async function getLocations(){
    const URL = "http://localhost:8000/api/locations/";
    let data;
    try {
      const res = await fetch(URL);
      if (res.ok) {
        data = await res.json();
      } else {
        throw new Error("Error: Problem getting the data 🥺");
      }
    } catch (e) {
      console.log(e.message);
    }
    // Add states to select dropdown
    const selectEl = document.querySelector("#location");
    for (let loc of data.locations) {
      let option = document.createElement("option");
      option.value = loc.id;
      option.innerHTML = loc.name;
      selectEl.appendChild(option);
    }
  }

  function handleNameChange(e){
    const value = e.target.value
    setName(value)
  }

  function handleStartsChange(e){
    const value = e.target.value
    setStarts(value)
  }
  function handleEndsChange(e){
    const value = e.target.value
    setEnds(value)
  }
  function handleDescriptionChange(e){
    const value = e.target.value
    setDescription(value)
  }
  function handleMaxPresentationsChange(e){
    const value = e.target.value
    setMaxPresentations(value)
  }
  function handleMaxAttendeesChange(e){
    const value = e.target.value
    setMaxAttendees(value)
  }
  function handleLocationChange(e){
    const value = e.target.value
    setLocation(value)
  }

  async function handleSubmit(e){
    e.preventDefault()
    const formTag = document.getElementById("create-conference-form");
    const formData = new FormData(formTag);
		const actualFormData = JSON.stringify(Object.fromEntries(formData));
		const options = {
			method: "POST",
			body: actualFormData,
			"Content-type": "application/json",
		};
		console.log("actual", actualFormData);
		const res = await fetch("http://localhost:8000/api/conferences/", options);
		if (res.ok) {
			const data = await res.json();
      console.log(data)
      setName('')
      setStarts('')
      setEnds('')
      setDescription('')
      setMaxPresentations('')
      setMaxAttendees('')
      setLocation('')
		}
  }

  useEffect(() => {
    getLocations();
  }, []);


  return (
    <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new conference</h1>
            <form onSubmit={handleSubmit} id="create-conference-form">
              <div className="form-floating mb-3">
                <input onChange={handleNameChange} value={name}placeholder="Name" required type="text" id="name" className="form-control" name="name"/>
                <label htmlFor="name">Name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleStartsChange} value={starts} placeholder="starts" required type="date" id="start_date" className="form-control" name="starts"/>
                <label htmlFor="start_date">Starts</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleEndsChange} value={ends} placeholder="Ends" required type="date" id="end_date" className="form-control" name="ends"/>
                <label htmlFor="end_date">Ends</label>
              </div>
              <label htmlFor="description">Description</label>
              <div className="form-floating mb-3">
                <textarea onChange={handleDescriptionChange} value={description} className="w-100" name="description" id="description" cols="30" rows="10"></textarea>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleMaxPresentationsChange} value={maxPresentations} placeholder="Maximum presentations" required type="number" id="max_pres" className="max_pres w-100 p-2" name="max_presentations"/>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleMaxAttendeesChange} value={maxAttendees} placeholder="Maximum attendees" required type="number" id="max_att" className="max_att w-100 p-2" name="max_attendees"/>
              </div>
              <div className="mb-3">
                <select onChange={handleLocationChange} value={location} required id="location" className="form-select" name="location">
                  <option value="">Choose a location</option>
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
  )
}
export default ConferenceForm
