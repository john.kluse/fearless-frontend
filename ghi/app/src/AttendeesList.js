function AttendeesList(props) {
  console.log('props', props)
  return (
        
        <table className={"table table-striped"}>
          <thead>
            <tr>
              <th>Name</th>
              <th>Conference</th>
            </tr>
          </thead>
          <tbody>
          {props.attendees.map(el => {
            return (
              <tr key={el.href}>
                <td>{ el.name }</td>
                <td>{ el.conference }</td>
              </tr>
            )
          })}
          </tbody>
        </table>
  )
}
export default AttendeesList
