
import {useState, useEffect} from 'react'

function AttendConference() {
const [conferences, setConferences] = useState('')
const [name, setName] = useState('')
const [email, setEmail] = useState('')
const [conference, setConference] = useState({})


  const fetchData = async () => {
    const url = 'http://localhost:8000/api/conferences/';

    try {
      const response = await fetch(url);

      if (!response.ok) {
        throw new Error(`HTTP error! Status: ${response.status}`);
      }

      const data = await response.json();
      console.log(data)
      await setConferences(data.conferences)
    } catch (error) {
      console.error('Error fetching data:', error.message);
    }
  };

  useEffect(() => {
    fetchData();
  }, []);

  async function handleSubmit(e){
    e.preventDefault()

    const data = {
      "email": email,
      "name": name,
      "conference": Number(conference),
  }
  console.log('data', data)
    const locationUrl = 'http://localhost:8001/api/attendees/';
    const fetchConfig = {
    method: "post",
    body: JSON.stringify(data),
    headers: {
      'Content-Type': 'application/json',
    },
  };


  const response = await fetch(locationUrl, fetchConfig);
  if (response.ok) {
    const newLocation = await response.json();
    console.log(newLocation);
    setConference('')
    setName('')
    setEmail('')
  }
}

function handleNameChange(e){
  setName(e.target.value)
}

function handleEmailChange(e){
  setEmail(e.target.value)
}

function handleConferenceChange(e){
  setConference(e.target.value)
}

  return (conferences &&
    <div class="row my-5 container">
        <div class="offset-3 col-6">
          <div class="shadow p-4 mt-4">
            <h1>It's Conference Time!</h1>
            <p>Please choose which conference you'd like to attend</p>
            <form onSubmit={handleSubmit} id="create-location-form">
            <div class="mb-3">
                <select onChange={handleConferenceChange} required id="conference" class="form-select" name="conference">
                  <option selected value="">Choose a conference</option>
                  {conferences.map((conference, idx) => {
                    return (
                      <option data-id={conference.id}  key={conference.id} value={idx}>{conference.name}</option>
                    )
                  })}
                </select>
              </div>
              <p>Now, tell us about yourself.</p>
              <div class="form-floating mb-3">
                <input onChange={handleNameChange} value={name} placeholder="Name" required type="text" id="name" class="form-control" name="name"/>
                <label for="name">Your full name</label>
              </div>
              <div class="form-floating mb-3">
                <input onChange={handleEmailChange} value={email} placeholder="Your email addr" required type="text" id="email" class="form-control" name="email"/>
                <label for="city">Your email address</label>
              </div>

              <button class="btn btn-primary">I'm going!</button>
            </form>
          </div>
        </div>
      </div>
  )
}
export default AttendConference
