import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import App from './App';

const root = ReactDOM.createRoot(document.getElementById('root'));
// root.render(
//   <React.StrictMode>
//     <App />
//   </React.StrictMode>
// );

async function getAttendees(){
  const response = await fetch('http://localhost:8001/api/attendees/');

  if (!response.ok){
    console.error('💥 Problem fetching data 💥')

  } else {
    const data = await response.json()
    root.render(
      <React.StrictMode>
        <App attendees={data.attendees}/>
      </React.StrictMode>
    );
  }
}
getAttendees()
