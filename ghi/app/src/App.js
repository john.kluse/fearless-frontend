import Nav from "./Nav";
import AttendeesList from "./AttendeesList";
import LocationForm from "./LocationForm";
import ConferenceForm from "./ConferenceForm";
import AttendConference from "./AttendConference";
import PresentationForm from "./PresentationForm";
import { BrowserRouter, Routes, Route } from
'react-router-dom'
import MainPage from "./MainPage"

function App(props) {
  if (props.attendees === undefined) {
    return null;
  }
    return (
      <BrowserRouter BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route index element={<MainPage />} />
          <Route path="locations">
            <Route path="new" element={<LocationForm />} />
          </Route>
          <Route path="conferences">
            <Route path="new" element={<ConferenceForm />}/>
          </Route>
          <Route path="attendees">
            <Route path="" element={<AttendeesList attendees={props.attendees}/>}/>
            <Route path="new" element={<AttendConference />}/>
          </Route>
            <Route path="presentations" element={<PresentationForm />} />
        </Routes>
      </div>
      </BrowserRouter >
    );
}

export default App;
