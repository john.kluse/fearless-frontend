window.addEventListener("DOMContentLoaded", async () => {
  const URL = 'http://localhost:8000/api/states/'
  let data
  try {
    const res = await fetch(URL)
    if (res.ok){
      data = await res.json()
    } else {
      throw new Error("Error: Problem getting the data 🥺")
    }
  } catch (e){
    console.log(e.message)
  }

  // Add states to select dropdown
    const selectEl = document.querySelector('#state')
    for (let state of data.states){
      let option = document.createElement('option')
      option.value = state.abbreviation
      option.innerHTML = state.name
      selectEl.appendChild(option)
    }

    const formTag = document.getElementById('create-location-form');

    formTag.addEventListener('submit', async event => {
    event.preventDefault();

    const formData = new FormData(formTag);
    const actualFormData = JSON.stringify(Object.fromEntries(formData))
    const options = {
      method: "POST",
      body: actualFormData,
      "Content-type": "application/json"

    }
    const res = await fetch('http://localhost:8000/api/locations/', options)
    if (res.ok){
      const data = await res.json()
      formTag.reset()
    }

  });

})
