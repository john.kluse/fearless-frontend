window.addEventListener("DOMContentLoaded", async () => {
	const URL = "http://localhost:8000/api/locations/";
	let data;
	try {
		const res = await fetch(URL);
		if (res.ok) {
			data = await res.json();
			console.log(data);
		} else {
			throw new Error("Error: Problem getting the data 🥺");
		}
	} catch (e) {
		console.log(e.message);
	}

	// Add states to select dropdown
	const selectEl = document.querySelector("#location");
	for (let loc of data.locations) {
		let option = document.createElement("option");
		option.value = loc.id;
		option.innerHTML = loc.name;
		selectEl.appendChild(option);
	}

	const formTag = document.getElementById("create-conference-form");
	console.log(formTag);

	formTag.addEventListener("submit", async (event) => {
		event.preventDefault();

		const formData = new FormData(formTag);
		const actualFormData = JSON.stringify(Object.fromEntries(formData));
		const options = {
			method: "POST",
			body: actualFormData,
			"Content-type": "application/json",
		};
		console.log("actual", actualFormData);
		const res = await fetch("http://localhost:8000/api/conferences/", options);
		if (res.ok) {
			const data = await res.json();
			formTag.reset();
		}
	});
});
