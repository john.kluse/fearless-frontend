function createCard(name, description, pictureUrl, start, end, loc) {
	return `
  <div class="col my-8">
    <div class="card shadow-lg ">
    <img src="${pictureUrl}" class="card-img-top ">
      <div class="card-body">
      <h5 class="card-title">${name}</h5>
      <h6 class="card-subtitle mb-2 text-muted">${loc}</h6>
      <p class="card-text">${description}</p>
      </div>
    </div>
    <div class="card-footer">
    <small class="text-muted">${start.toLocaleDateString("en-US")} - ${end.toLocaleDateString("en-US")}</small>

  </div>
  </div>
  `;
}

window.addEventListener("DOMContentLoaded", async () => {
	const url = "http://localhost:8000/api/conferences/";

	try {
		const response = await fetch(url);

		if (!response.ok) {
			// Figure out what to do when the response is bad
		} else {
			const data = await response.json();

			for (let conference of data.conferences) {
				const detailUrl = `http://localhost:8000${conference.href}`;
				
				const detailResponse = await fetch(detailUrl);
				if (detailResponse.ok) {
					const details = await detailResponse.json();
					const title = details.conference.name;
					const description = details.conference.description;
					const pictureUrl = details.conference.location.picture_url;
					const start = new Date(details.conference.starts);
					const end = new Date(details.conference.ends);
          const loc = details.conference.location.name
					const html = createCard(title, description, pictureUrl, start, end, loc);
					const row = document.querySelector(".row");
					row.innerHTML += html;
				}
			}
		}
	} catch (e) {
		console.log(e.message)
	}
});
